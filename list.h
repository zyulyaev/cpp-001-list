#ifndef LIST_H
#define	LIST_H

#include <cstdlib>
#include <exception>
#include <algorithm>

class iterator_out_of_bound_exception : public std::exception {
public:
    ~iterator_out_of_bound_exception() throw() {
    }
    const char* what() throw() {
        return "Iterator out of bound";
    }
};

template <class T> class list {
private:
    class node {
    public:
        T value;
        node* left;
        node* right;
        
        node(const T &value, node* left, node* right) :
                value(value), left(left), right(right) {
        }
    };
    
    node* _left;
    node* _right;
    size_t _size;
    
public:
    list() {
        _left = _right = new node(T(), 0, 0);
        _size = 0;
    }
    
    list(const list& l) {
        _left = _right = new node(T(), 0, 0);
        _size = 0;
        for (const_iterator it = l.begin(); it != l.end(); ++it) {
            push_back(*it);
        }
    }
    
    ~list() {
        node *c = _left;
        while (c != 0) {
            node *next = c->right;
            delete c;
            c = next;
        }
    }

    void swap(list& l) {
        std::swap(_left, l._left);
        std::swap(_right, l._right);
        std::swap(_size, l._size);
    }
    
    list& operator=(list l) {
        swap(l);
        return *this;
    }
    
    void clear() {
        list dummy;
        swap(dummy);
    }
    
    void push_back(const T& value) {
        insert(end(), value);
    }
    
    void pop_back() {
        erase(--end());
    }
    
    T const& back() const {
        return *(--end());
    }
    
    void push_front(const T& value) {
        insert(begin(), value);
    }
    
    void pop_front() {
        erase(begin());
    }
    
    T const& front() const {
        return *begin();
    }
    
    class iterator : public std::iterator<std::bidirectional_iterator_tag, node> {
    public:
        node* _node;
        
        iterator(node* _node) : _node(_node) {}
        iterator(const iterator& it) : _node(it._node) {}
        
        bool operator==(const iterator &it) const {
            return _node == it._node;
        }
        
        bool operator!=(const iterator &it) const {
            return _node != it._node;
        }
        
        T& operator*() {
            return _node->value;
        }
        
        iterator& operator++() {
            if (!_node->right) {
                throw iterator_out_of_bound_exception();
            }
            _node = _node->right;
            return *this;
        }

        iterator operator++(int) {
            iterator tmp(*this); 
            operator++(); 
            return tmp;
        }
        
        iterator& operator--() {
            if (!_node->left) {
                throw iterator_out_of_bound_exception();
            }
            _node = _node->left;
            return *this;
        }

        iterator operator--(int) {
            iterator tmp(*this); 
            operator--(); 
            return tmp;
        }
    };

    class const_iterator : public std::iterator<std::bidirectional_iterator_tag, const node> {
    public:
        const node* _node;

        const_iterator(const node* _node) : _node(_node) {}
        const_iterator(const const_iterator& it) : _node(it._node) {}

        bool operator==(const const_iterator& it) const {
            return _node == it._node;
        }

        bool operator!=(const const_iterator& it) const {
            return _node != it._node;
        }

        T const& operator*() const {
            return _node->value;
        }

        const_iterator& operator++() {
            if (!_node->right) {
                throw iterator_out_of_bound_exception();
            }
            _node = _node->right;
            return *this;
        }

        const_iterator operator++(int) {
            const_iterator tmp(*this); 
            operator++(); 
            return tmp;
        }

        const_iterator& operator--() {
            if (!_node->left) {
                throw iterator_out_of_bound_exception();
            }
            _node = _node->left;
            return *this;
        }

        const_iterator operator--(int) {
            const_iterator tmp(*this); 
            operator--(); 
            return tmp;
        }
    };
    
    const_iterator begin() const {
        return const_iterator(_left);
    }
    iterator begin() {
        return iterator(_left);
    }
    
    const_iterator end() const {
        return const_iterator(_right);
    }
    iterator end() {
        return iterator(_right);
    }
    
    void erase(const iterator& it) {
        node* c = it._node;
        if (c == _right) {
            throw iterator_out_of_bound_exception();
        }
        if (c->left) {
            c->left->right = c->right;
        }
        if (c->right) {
            c->right->left = c->left;
        }
        if (c == _left) {
            _left = c->right;
        }
        --_size;
        delete c;
    }
    
    iterator insert(const iterator& it, const T& value) {
        node* c = it._node;
        node* n = new node(value, c->left, c);
        if (c->left) {
            c->left->right = n;
        }
        c->left = n;
        if (c == _left) {
            _left = n;
        }
        ++_size;
        return iterator(n);
    }

    size_t size() const {
        return _size;
    }
};

#endif

