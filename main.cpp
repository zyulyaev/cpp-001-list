#include "list.h"

#define BOOST_TEST_MODULE
#include <boost/test/included/unit_test.hpp>
#include <boost/utility.hpp>

list<int> l;

BOOST_AUTO_TEST_CASE(test_fill) {
    l.push_back(1);
    l.push_front(2);
    l.push_back(3);
    l.push_front(4);
    BOOST_CHECK_EQUAL(*l.begin(), 4);
    BOOST_CHECK_EQUAL(*boost::next(l.begin()), 2);
    BOOST_CHECK_EQUAL(*boost::next(l.begin(), 2), 1);
    BOOST_CHECK_EQUAL(*boost::next(l.begin(), 3), 3);
}

BOOST_AUTO_TEST_CASE(test_push_back) {
    l.push_back(5);
    BOOST_CHECK_EQUAL(l.back(), 5);
}

BOOST_AUTO_TEST_CASE(test_push_front) {
    l.push_front(6);
    BOOST_CHECK_EQUAL(l.front(), 6);
}

BOOST_AUTO_TEST_CASE(test_pop_back) {
    l.push_back(7);
    l.push_back(8);
    l.pop_back();
    BOOST_CHECK_EQUAL(l.back(), 7);
}

BOOST_AUTO_TEST_CASE(test_pop_front) {
    l.push_front(9);
    l.push_front(10);
    l.pop_front();
    BOOST_CHECK_EQUAL(l.front(), 9);
}

void equal(const list<int>& a, const list<int> &b) {
    for (list<int>::const_iterator i = a.begin(), j = b.begin(); i != a.end() || j != b.end(); ++i, ++j) {
        BOOST_CHECK_EQUAL(*i, *j);
    }
    BOOST_CHECK_EQUAL(a.size(), b.size());
}

BOOST_AUTO_TEST_CASE(test_copy) {
    list<int> r(l);   
    equal(r, l);
}

BOOST_AUTO_TEST_CASE(test_assign) {
    list<int> r;
    r = l;
    equal(r, l);
}

BOOST_AUTO_TEST_CASE(test_erase) {
    l.push_back(11);
    l.push_back(12);
    l.push_back(13);
    l.push_back(14);
    l.erase(boost::prior(l.end(), 3));
    l.erase(boost::prior(l.end(), 2));
    BOOST_CHECK_EQUAL(l.back(), 14);
    l.erase(boost::prior(l.end()));
    BOOST_CHECK_EQUAL(l.back(), 11);
}

BOOST_AUTO_TEST_CASE(test_insert) {
    l.push_front(15);
    l.push_front(16);
    l.insert(boost::next(l.begin()), 17);
    BOOST_CHECK_EQUAL(l.front(), 16);
    l.pop_front();
    BOOST_CHECK_EQUAL(l.front(), 17);
    l.pop_front();
    BOOST_CHECK_EQUAL(l.front(), 15);
}

BOOST_AUTO_TEST_CASE(test_size) {
    size_t s = l.size();
    l.push_back(18);
    BOOST_CHECK_EQUAL(l.size(), s + 1);
    l.push_front(19);
    BOOST_CHECK_EQUAL(l.size(), s + 2);
    l.insert(boost::next(l.begin()), 20);
    BOOST_CHECK_EQUAL(l.size(), s + 3);
    l.erase(boost::prior(l.end(), 2));
    BOOST_CHECK_EQUAL(l.size(), s + 2);
    l.pop_front();
    BOOST_CHECK_EQUAL(l.size(), s + 1);
    l.pop_back();
    BOOST_CHECK_EQUAL(l.size(), s);
    l.clear();
    BOOST_CHECK_EQUAL(l.size(), 0);
}

BOOST_AUTO_TEST_CASE(test_exceptions) {
    BOOST_CHECK_THROW(l.erase(l.end()), iterator_out_of_bound_exception);
    BOOST_CHECK_THROW(boost::next(l.end()), iterator_out_of_bound_exception);
    BOOST_CHECK_THROW(boost::prior(l.begin()), iterator_out_of_bound_exception);
}

BOOST_AUTO_TEST_CASE(test_mine) {
    l.push_front(100);
    const list<int> r = l;
    list<int>::const_iterator it = r.begin();
    long long x = (long long)&(*it);
    *((int*)x) = 200;
//    BOOST_CHECK_EQUAL(r.front(), 100);
}